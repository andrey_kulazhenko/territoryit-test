Rails.application.routes.draw do

  scope :api do
    scope :v1 do
      post  'posts',                to: 'posts#create'
      post  'posts/:post_id/rate',  to: 'posts#rate'
      get   'posts/ips',            to: 'posts#ips'
      get   'posts/top',            to: 'posts#top'
    end
  end

end
