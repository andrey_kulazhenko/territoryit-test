class PostService
  def self.create(params)
    user = User.find_or_create_by(login: params[:author])
    post = Post.create params.merge(user: user)
    Result.new post, PostSerializer
  end

  def self.rate(params)
    rate = PostRate.create params
    Result.new rate, RateSerializer
  end

  def self.ips
    Post.select('ip, ARRAY_AGG(DISTINCT author) as logins').group(:ip).having('count(id) > 1 AND ip IS NOT NULL')
  end

  def self.top(limit)
    Post.joins(:post_rates).group('posts.id').order('AVG(rate) DESC').limit(limit)
  end
end
