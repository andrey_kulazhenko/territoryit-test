class Result
  attr_reader :obj
  attr_reader :success_serializer
  attr_reader :error_serializer

  def initialize(obj, success_serializer, error_serializer = ErrorSerializer)
    @obj = obj
    @success_serializer = success_serializer
    @error_serializer = error_serializer
  end

  def status
    obj.valid? ? :ok : :unprocessable_entity
  end

  def serializer
    obj.valid? ? success_serializer : error_serializer
  end
end