class PostsController < ApplicationController

  def create
    result = PostService.create post_params
    render json: result.obj, serializer: result.serializer, status: result.status
  end

  def rate
    result = PostService.rate rate_params
    render json: result.obj, serializer: result.serializer, status: result.status
  end

  def ips
    data = PostService.ips
    render json: data, each_serializer: IpsSerializer, status: :ok
  end

  def top
    posts = PostService.top params[:limit]
    render json: posts, each_serializer: PostSerializer, status: :ok
  end

  private

  def post_params
    params.permit(:title, :description, :author, :ip)
  end

  def rate_params
    params.permit(:rate, :post_id)
  end
end