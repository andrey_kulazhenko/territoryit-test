class RateSerializer < ActiveModel::Serializer
  attributes :avg_rate

  def avg_rate
    PostRate.where(post_id: object.post_id).average(:rate)
  end
end
