class PostSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :ip, :author, :created_at, :updated_at
end
