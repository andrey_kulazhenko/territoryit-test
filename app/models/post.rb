class Post < ApplicationRecord
  belongs_to :user
  has_many :post_rates

  validates :title, :description, :author, presence: true
  validates_presence_of :user
end
