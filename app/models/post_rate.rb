class PostRate < ApplicationRecord
  belongs_to :post

  validates_presence_of :post
  validates_presence_of :rate
  validates :rate, numericality: { less_than_or_equal_to: 5, greater_than_or_equal_to: 0,  only_integer: true }
end
