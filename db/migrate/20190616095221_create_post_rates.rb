class CreatePostRates < ActiveRecord::Migration[5.2]
  def change
    create_table :post_rates do |t|
      t.integer :rate
      t.references :post, foreign_key: true

      t.timestamps
    end
  end
end
