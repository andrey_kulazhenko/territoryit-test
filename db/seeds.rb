# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
ips = Array.new(50) { Faker::Internet.ip_v4_address }
100.times do
  user = User.create login: Faker::Internet.username
  2000.times do
    post = Post.create title: Faker::Lorem.sentence(3), description: Faker::Lorem.paragraph(3), user: user, author: user.login, ip: ips.sample
    rand(5..20).times do
      post.post_rates.create rate: rand(0..5)
    end if [true, false].sample
  end
end