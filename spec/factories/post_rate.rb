FactoryBot.define do
  factory :post_rate do
    rate { rand(1..5) }
  end
end