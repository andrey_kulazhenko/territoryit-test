FactoryBot.define do
  factory :post do
    title { Faker::Lorem.sentence(3) }
    description { Faker::Lorem.paragraph(3) }
    ip { Faker::Internet.ip_v4_address }
    author { user.login }
    association :user, factory: :user

    transient do
      rates_count { 10 }
    end

    trait :with_rate do
      after(:create) do |post, evaluator|
        create_list :post_rate, evaluator.rates_count, post: post
      end
    end

    trait :top_rate do
      after(:create) do |post, evaluator|
        create_list :post_rate, evaluator.rates_count, post: post, rate: 5
      end
    end
  end
end