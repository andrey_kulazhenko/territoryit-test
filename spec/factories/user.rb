FactoryBot.define do
  factory :user do
    login { Faker::Internet.username }

    transient do
      posts_count { 10 }
    end

    after(:create) do |user, evaluator|
      create_list :post, evaluator.posts_count, :with_rate, user: user
    end

    trait :posts_with_static_ip do
      after(:create) do |user|
        create_list :post, 5, user: user, ip: '192.168.1.1'
      end
    end

    trait :posts_with_top_rate do
      after(:create) do |user|
        create :post, :top_rate, user: user
      end
    end
  end
end