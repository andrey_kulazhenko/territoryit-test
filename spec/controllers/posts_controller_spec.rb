require 'rails_helper'

RSpec.describe PostsController do

  describe 'GET ips' do
    before(:all) do
      create_list :user, 3, :posts_with_static_ip
    end

    it 'should fetch ips' do
      get :ips
      expect(response.status).to eql(200)
      hash_body = JSON.parse(response.body).first.with_indifferent_access
      expect(hash_body).to include(:ip, :logins)
      expect(hash_body[:logins]).to match_array(User.pluck(:login))
    end
  end

  describe 'GET top' do
    before(:all) do
      create :user, :posts_with_top_rate
    end

    it 'should fetch top' do
      get :top
      expect(response.status).to eql(200)
      hash_body = JSON.parse(response.body).first.with_indifferent_access
      expect(hash_body).to include(:id, :title, :description, :ip, :author)
      expect(hash_body[:id]).to eql(Post.last.id)
    end

    it 'should fetch top with limit' do
      get :top, params: {
          limit: 0
      }
      expect(response.status).to eql(200)
      hash_body = JSON.parse(response.body)
      expect(hash_body).to be_empty
    end
  end

  describe 'POST create' do

    it 'should create post' do
      post :create, params: {
          title: 'New post title',
          description: 'New post description',
          author: 'New author'
      }
      expect(response.status).to eql(200)
      hash_body = JSON.parse(response.body).with_indifferent_access
      expect(hash_body).to include(:id, :title, :description, :ip, :author)
      expect(hash_body).to include(title: 'New post title', description: 'New post description')
    end

    it 'should raise validation error' do
      post :create, params: {
          title: nil,
          description: 'New post description',
          author: 'New author'
      }
      expect(response.status).to eql(422)
      hash_body = JSON.parse(response.body).with_indifferent_access
      expect(hash_body).to include(:errors)
    end

    it 'should raise user validation error' do
      post :create, params: {
          title: 'New post title',
          description: 'New post description',
          author: nil,
      }
      expect(response.status).to eql(422)
      hash_body = JSON.parse(response.body).with_indifferent_access
      expect(hash_body).to include(:errors)
    end
  end

  describe 'POST rate' do

    let!(:post_obj) { create(:post) }

    it 'should rate post' do
      post :rate, params: {
        rate: 5,
        post_id: post_obj.id
      }
      expect(response.status).to eql(200)
      hash_body = JSON.parse(response.body).with_indifferent_access
      expect(hash_body).to include(:avg_rate)
      expect(hash_body[:avg_rate].to_f).to eql(5.0)
    end

    it 'should raise validation error if rate wrong' do
      post :rate, params: {
        rate: 10,
        post_id: post_obj.id
      }
      expect(response.status).to eql(422)
      hash_body = JSON.parse(response.body).with_indifferent_access
      expect(hash_body).to include(:errors)
    end

    it 'should raise validation error if post not exists' do
      post :rate, params: {
        rate: 5,
        post_id: 100500
      }
      expect(response.status).to eql(422)
      hash_body = JSON.parse(response.body).with_indifferent_access
      expect(hash_body).to include(:errors)
    end
  end

end