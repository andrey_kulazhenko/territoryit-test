# README

1. copy `app_config.yml.sample` to `app_config.yml` and provide right configs
1. run `bundle exec fastball config` to generate config fies
1. run `bundle exec rake db:setup` to initialize DB
1. done! 

To run tests: `bundle exec rspec spec`